FROM rabbitmq:3.7.5-management

RUN apt-get update \
    && apt-get install -y python curl \
    && curl https://raw.githubusercontent.com/rabbitmq/rabbitmq-management/rabbitmq_v3_7_5/bin/rabbitmqadmin -o /usr/bin/rabbitmqadmin \
    && chmod +x /usr/bin/rabbitmqadmin \
    && rm -r /var/lib/apt/lists/*
